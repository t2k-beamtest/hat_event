#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#include <vector>
#pragma link C++ class vector<int>;
#pragma link C++ class vector<vector<int> > ;
#pragma link C++ class vector<short>;
#pragma link C++ class vector<vector<short> > ;
#pragma link C++ class vector<vector<double> >;

#pragma link C++ class TRawHit+;
#pragma link C++ class std::vector<TRawHit>+;
#pragma link C++ class std::vector<std::shared_ptr<TRawHit>>+;
#pragma link C++ class TRawEvent+;

#endif