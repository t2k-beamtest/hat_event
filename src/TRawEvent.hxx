#ifndef SRC_CLASS_TRAWEVENT_HXX_
#define SRC_CLASS_TRAWEVENT_HXX_

#include "TRawHit.hxx"

//! Class that contains pointers to all the hits in the event

//! This class is used for the I/O with a ROOT file
//! Pedestals are NOT subtracted in the waveforms
class TRawEvent : public TObject {
 public:
    //ctor
    explicit TRawEvent() : ID(0) {}
    explicit TRawEvent(Int_t var) : ID(var) {}
    explicit TRawEvent(const TRawEvent *event) : fHits(event->fHits),
                                                 ID(event->GetID()) {};

    ~TRawEvent() {
        for (auto & fHit : fHits) {
            if (fHit) {
                delete fHit;
                fHit = nullptr;
            }
        }
        fHits.clear();
    }

    // getters
    UInt_t GetID() const { return ID; }
    std::vector<TRawHit*> GetHits() const { return fHits; }
    ushort GetTimeMid() const {return TimeStampMid;}
    ushort GetTimeMsb() const {return TimeStampMsb;}
    ushort GetTimeLsb() const {return TimeStampLsb;}
    // setters
    void SetHits(const std::vector<TRawHit*> &hits) { fHits = hits; }
    void SetID(Int_t var) { ID = var; }
    void SetTime(ushort Mid, ushort Msb, ushort Lsb) {TimeStampMid = Mid; TimeStampMsb = Msb; TimeStampLsb = Lsb;}
    void Reserve(size_t s) {fHits.reserve(s);}
    void AddHit(TRawHit* hit) { fHits.emplace_back(hit); }

 ClassDef (TRawEvent, 1);

 protected:
    /// Event Id
    UInt_t ID;

    /// time stamp
    ushort TimeStampMid{0};
    ushort TimeStampMsb{0};
    ushort TimeStampLsb{0};
    /// vector of hits in event
    std::vector<TRawHit*> fHits;
};

#endif