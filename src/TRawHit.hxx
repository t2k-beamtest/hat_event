#ifndef SRC_CLASS_TRAWHIT_HXX_
#define SRC_CLASS_TRAWHIT_HXX_

/** @cond */
#include "TROOT.h"
#include <iostream>
/** @endcond */

class TRawHit;
/// shared pointer to THit
using TRawHitPtr = std::shared_ptr<TRawHit>;
/// vector of shared pointers to THit, e.g. event or cluster
using TRawHitPtrVec = std::vector<TRawHitPtr>;

//! Class for storing information about each reconstructed hit.

//! Store row, column, charge, time and the whole waveform
//! Pedestals are NOT subtracted in the waveforms
class TRawHit : public TObject {
 public:
    // ctor
    explicit TRawHit(const short card = -1,
                     const short chip = -1,
                     const short channel = -1,
                     const short time = 0) :
        fCard(card), fChip(chip), fChannel(channel), fTime(time) {
        fwf.clear();
    }

    explicit TRawHit(const TRawHit *rhs) : fCard(rhs->fCard),
                                           fChip(rhs->fChip),
                                           fChannel(rhs->fChannel),
                                           fTime(rhs->fTime),
                                           fwf(rhs->fwf) {}

    /// equality operator --> same pad disregard (!!!) the WF
    bool operator==(const TRawHit& rhs) const {
        return fCard == rhs.fCard && fChip == rhs.fChip && fChannel == rhs.fChannel;
    }

    /// setters
    void SetCard(short card) {fCard = card;}
    void SetChip(short chip) {fChip = chip;}
    void SetChannel(short channel) {fChannel = channel;}
    void SetTime(short time) { fTime = time; }
    template<typename T>
    void SetADCvector(T&& v) {
        fwf = std::forward<T>(v);
    }
    void SetADCunit(short pos, short ampl) {
        if (pos >= fwf.size()) {
//            std::cerr << "ADC index out of range!\t" << pos << std::endl;
            return;
        }
        fwf[pos] = ampl;
    }
    void ResetWF() {
        fwf.clear();
        fwf.resize(514, 0);
        fTime = 0;
    }

    void ShrinkWF() {
        for (short i = 0; i < fwf.size(); ++i) {
            if (fwf[i] != 0) {
                fTime = i;
                // smell. Unfortunately, std::advance is available only from C++17
                fwf.erase(fwf.begin(), fwf.begin() +  i);
                break;
            }
        }

        for (short i = fwf.size() - 1; i > 0; --i) {
            if (fwf[i] != 0) {
                fwf.erase(fwf.begin() + i + 1, fwf.end());
                break;
            }
        }

        fwf.shrink_to_fit();
    }

    /// getters
    short GetTime() const { return fTime; }
    short GetADC(int i) const {
        if (i >= 0 && i < 512 && fwf.size() > i - fTime)
            return fwf[i - fTime];
        std::cerr << "ADC index out of range!\t" << i << std::endl;
        return 0;
    }
    std::vector<short> GetADCvector() const {
        return fwf;
    }
    short GetCard() const { return fCard; }
    short GetChip() const { return fChip; }
    short GetChannel() const { return fChannel; }

 ClassDef (TRawHit, 1);

 private:
    short fCard;
    short fChip;
    short fChannel;

    short fTime;
    std::vector<short> fwf;
};

#endif
