# Event structure for the HAT DAQ

## HOW TO

1. Add submodule to project
```bash
git submodule add https://git_link external/hat_event
 ```

2. Update CMakeLists file of your project with the following:
```CMake
set(PBUILDER OFF)
include_directories (
        ${ROOT_INCLUDE_DIR}
        # ${PROJECT_SOURCE_DIR}/external/hat_event
        ${PROJECT_SOURCE_DIR}/external/hat_event/src
)

add_subdirectory (external/hat_event)
```

If you build your project with pBuilder framework add the following

```Cmake
set(PBUILDER ON)
list(APPEND CMAKE_MODULE_PATH
        ${PROJECT_SOURCE_DIR}/cmake
        ${PROJECT_SOURCE_DIR}/external/hat_event/cmake)
include(PackageBuilder)

pbuilder_add_submodule(hat_event ${PROJECT_SOURCE_DIR}/external/hat_event)

include_directories (
        # ${PROJECT_SOURCE_DIR}/external/hat_event
        ${PROJECT_SOURCE_DIR}/external/hat_event/src
)
```