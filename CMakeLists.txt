cmake_minimum_required(VERSION 3.1)

set(PROJECT_NAME hat_event)
set(PROJECT_VERSION 1.0)

project(${PROJECT_NAME} VERSION ${PROJECT_VERSION})

# From https://blog.kitware.com/cmake-and-the-default-build-type/
# Set a default build type to Release if none was specified
# This has to be before Scarab PackageBuilder since the package builder defines the default mode to DEBUG...
set(default_build_type "Release")
if (NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
    message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
    set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
            STRING "Choose the type of build." FORCE)
    # Set the possible values of build type for cmake-gui
    set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
            "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif ()

list(APPEND CMAKE_MODULE_PATH
        ${PROJECT_SOURCE_DIR}/cmake)
include(PackageBuilder)

MESSAGE(STATUS ${CMAKE_MODULE_PATH})

if (PBUILDER)
    message(STATUS "TRawEvent dictionary with pbuilder...")
    pbuilder_prepare_project()

    set(CMAKE_CXX_STANDARD 14)

    #---Locate the ROOT package and defines a number of variables (e.g. ROOT_INCLUDE_DIRS)
    find_package(ROOT REQUIRED COMPONENTS RIO Net)
    if (ROOT_FOUND)

    else (ROOT_FOUND)
        message(FATAL "Unable to find ROOT")
    endif (ROOT_FOUND)

    # include(${ROOT_USE_FILE})
    include_directories (${ROOT_INCLUDE_DIR})
    LIST(APPEND PUBLIC_EXT_LIBS ${ROOT_LIBRARIES})

    if(ROOT_CXX_FLAGS MATCHES "-std=c\\+\\+17")
        set(CMAKE_CXX_STANDARD 17)
    endif()

    message(STATUS "TRawEvent dictionary ...")

    #---Generate dictionaries
    set (EVENT_DICT_HEADERFILES
            TRawEvent.hxx
            TRawHit.hxx
            )

    include_directories(src/)
    # include_directories(./)

    set( EVENT_SOURCEFILES ${EVENT_SOURCEFILES} ${CMAKE_CURRENT_BINARY_DIR}/TRawEventDict.cxx )
    set( EVENT_DICT_PCMFILE ${CMAKE_CURRENT_BINARY_DIR}/TRawEventDict_rdict.pcm )

    ##################################################
    ROOT_GENERATE_DICTIONARY( TRawEventDict ${EVENT_DICT_HEADERFILES} LINKDEF linkDef.h  OPTIONS -inlineInputHeader )

    pbuilder_install_files(${LIB_INSTALL_DIR} ${EVENT_DICT_PCMFILE})

    # ###########
    # # Library #
    # ###########

    pbuilder_library(
            TARGET TRawEvent
            SOURCES ${EVENT_SOURCEFILES}
            PUBLIC_EXTERNAL_LIBRARIES ${PUBLIC_EXT_LIBS}
            PRIVATE_EXTERNAL_LIBRARIES ${PRIVATE_EXT_LIBS}
    )
    pbuilder_component_install_and_export(
            COMPONENT Library
            LIBTARGETS TRawEvent
    )

    configure_file(${PROJECT_SOURCE_DIR}/hat_eventConfig.cmake.in ${CMAKE_CURRENT_BINARY_DIR}/hat_eventConfig.cmake @ONLY)
    pbuilder_do_package_config()

    pbuilder_install_headers(${EVENT_DICT_HEADERFILES})

else()
    message(STATUS "TRawEvent dictionary without pbuilder...")
    find_package(ROOT REQUIRED COMPONENTS RIO Net)
    #---Define useful ROOT functions and macros (e.g. ROOT_GENERATE_DICTIONARY)
    #    include(${ROOT_USE_FILE})
    include_directories (${ROOT_INCLUDE_DIR})
    set(CMAKE_CXX_STANDARD 14)
    if(ROOT_CXX_FLAGS MATCHES "-std=c\\+\\+17")
        set(CMAKE_CXX_STANDARD 17)
    endif()
    #    include(${ROOT_USE_FILE})
    #---Generate dictionaries
    set (EVENT_DICT_HEADERFILES
            TRawEvent.hxx
            TRawHit.hxx
            )

    include_directories(src/)
    # include_directories(./)

    set( EVENT_SOURCEFILES ${EVENT_SOURCEFILES} ${CMAKE_CURRENT_BINARY_DIR}/TRawEventDict.cxx )

    ROOT_GENERATE_DICTIONARY(TRawEventDict ${EVENT_DICT_HEADERFILES} LINKDEF linkDef.h)
    add_library(TRawEvent ${EVENT_SOURCEFILES})
    target_include_directories(TRawEvent INTERFACE src/ )
    target_link_libraries(TRawEvent ${ROOT_LIBRARIES})

    install(TARGETS TRawEvent)
    install(PROGRAMS ${CMAKE_CURRENT_BINARY_DIR}/TRawEventDict_rdict.pcm DESTINATION lib)
endif()
